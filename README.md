# Tasks

A new Flutter project.

## Getting Started
- Download Flutter for [Windows](https://flutter.io/docs/get-started/install/windows) OR [MacOS](https://flutter.io/docs/get-started/install/macos)

- You can use Android Studio for Visual Code for development. I prefer Visual Code [Download](https://code.visualstudio.com/download).

## Install the Flutter and Dart plugins

- After install Visual Code open it.
- Invoke **View > Command Palette…**
- Type **“install”, and select Extensions: Install Extensions**.
- Type “flutter” in the extensions search field, select **Flutter** in the list, and click **Install**. This also installs the required Dart plugin.

## Test drive
- Open Project in **Visual Code**
- To Run on MacOS cmd + f5
- To Run on Windows ctrl + f5

## Codebase

We have multiple folders at root level. But we only need to know about **lib**. In **lib** folder we have 3 files. 

### Root Project
This is folder lib at root level and it has Main.dart that is the starting point of the application.

### Tast1 Module
Task1 is a module that have 2 numeric input fields and a **Calculate** button that calculate the sum on input fields.

### Tast1 Module
Task2 is a module that have text input fields and a **Calculate** button that calculate the number of words in input text.