import 'package:flutter/material.dart';
import 'package:task1/task1.dart';
import 'package:task2/task2.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Assignment',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'Select Task'),
      routes: appRoutes(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: RaisedButton(
                  child: Text('Task 1',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                      )),
                  color: Colors.blue,
                  onPressed: () {
                    Navigator.pushNamed(context, "/TaskOnePage");
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: RaisedButton(
                  child: Text('Task 2 ',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                      )),
                  color: Colors.blue,
                  onPressed: () {
                    Navigator.pushNamed(context, "/TaskTwoPage");
                  },
                ),
              )
            ],
          ),
        ));
  }
}

Map<String, WidgetBuilder> appRoutes() {
  return {
    "/TaskOnePage": (BuildContext context) => new TaskOnePage(),
    "/TaskTwoPage": (BuildContext context) => new TaskTwoPage()
  };
}
