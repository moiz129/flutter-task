import 'package:flutter/material.dart';

class TaskOnePage extends StatefulWidget {
  TaskOnePage({Key key}) : super(key: key);
  final String title = "Task 1";

  @override
  _TaskOnePageState createState() => new _TaskOnePageState();
}

class _TaskOnePageState extends State<TaskOnePage> {
  final field1 = TextEditingController();
  final field2 = TextEditingController();

  double answer = 0.0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(12.0),
                  width: MediaQuery.of(context).size.width,
                  child: TextField(
                    controller: field1,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(labelText: 'Input 1'),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(12.0),
                  width: MediaQuery.of(context).size.width,
                  child: TextField(
                    controller: field2,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(labelText: 'Input 2'),
                  ),
                ),
                Container(
                  width: 100.0,
                  child: RaisedButton(
                    child: Text('Calculate',
                        style: TextStyle(color: Colors.white)),
                    color: Colors.blue,
                    onPressed: () {
                      print(field1.value);
                      print(field2.value);
                      setState(() {
                        answer = double.parse(field1.value.text) +
                            double.parse(field2.value.text);
                      });
                    },
                  ),
                ),
                Center(
                  child: Column(
                    children: <Widget>[
                      Text('Total: ${answer.toString()}',
                          style: TextStyle(
                            fontSize: 20.0,
                          ))
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
