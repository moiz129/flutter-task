import 'package:flutter/material.dart';

class TaskTwoPage extends StatefulWidget {
  TaskTwoPage({Key key}) : super(key: key);
  final String title = "Task 2";

  @override
  _TaskTwoPageState createState() => new _TaskTwoPageState();
}

class _TaskTwoPageState extends State<TaskTwoPage> {
  final field1 = TextEditingController();

  int len = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(12.0),
                  width: MediaQuery.of(context).size.width,
                  child: TextField(
                    controller: field1,
                    keyboardType: TextInputType.text,
                    decoration: new InputDecoration(labelText: 'Enter Text'),
                  ),
                ),
                Container(
                  width: 100.0,
                  child: RaisedButton(
                    child: Text('Calculate',
                        style: TextStyle(color: Colors.white)),
                    color: Colors.blue,
                    onPressed: () {
                      setState(() {
                        if (field1.value.text.trim().length > 0) {
                          len = field1.value.text.trim().split(" ").length;
                        } else {
                          len = 0;
                        }
                      });
                    },
                  ),
                ),
                Center(
                    child: Column(
                  children: <Widget>[
                    Text('Total Words: ' + len.toString(),
                        style: TextStyle(
                          fontSize: 20.0,
                        )),
                  ],
                ))
              ],
            ),
          ),
        ));
  }
}
